let mongoose = require('mongoose');

let Schema = mongoose.Schema;

let ProjectSchema = new Schema({
    name: { type: String, required: true },
    userID: {type: String, required: true},
    tasks: [{
        description: String,
        creationDate: Date,
        finishDate: Date,
        isDone: Boolean,
    }]
});

let ProjectModel = mongoose.model('Project', ProjectSchema);


async function create(values) {
    let newProject = ProjectModel(values);
    return await save(newProject);
}

async function update(id, body) {
    return await ProjectModel.updateOne({ "_id": id }, {"name": body.name});
}

async function remove(id) {
    return await ProjectModel.remove({ "_id": id });
}

async function getProjectsByUserId(userId) {
    return await ProjectModel.find({ "userID": userId });
}

async function addTaskToProject(projectId, body) {
    let task = {
        description: body.description,
        creationDate: new Date(),
        finishDate: null,
        isDone: false,
    }
    return await ProjectModel.findOneAndUpdate({ "_id": projectId }, { $push: { tasks: task } }).then((item) =>{ return task});
}

async function removeTaskToProject(projectId, taskId) {
    return await ProjectModel.updateOne({ "_id": projectId }, { $pull: { tasks: {"_id": taskId} } });
}

async function editTaskToProject(projectId, taskId, body) {
    return await ProjectModel.updateOne({ "_id": projectId, "tasks._id":  taskId}, {$set: { "tasks.$.description": body.description }});
}

async function finishTaskToProject(projectId, taskId) {
    return await ProjectModel.updateOne({ "_id": projectId, "tasks._id":  taskId}, {$set: { "tasks.$.finishDate": new Date(), "tasks.$.isDone": true}});
}

async function save(project) {
    return await project.save();
}

module.exports.create = create;
module.exports.update = update;
module.exports.remove = remove;
module.exports.getProjectsByUserId = getProjectsByUserId;
module.exports.addTaskToProject = addTaskToProject;
module.exports.removeTaskToProject = removeTaskToProject;
module.exports.editTaskToProject = editTaskToProject;
module.exports.finishTaskToProject = finishTaskToProject;