let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let UserSchema = new Schema({
    username: { type: String, required: true },
    password: { type: String, required: true }
});

let UserModel = mongoose.model('User', UserSchema);


async function create(values) {
    let newUser = UserModel(values);
    return await save(newUser);
}

async function login(user) {
    return await UserModel.find(user);
}


async function save(user) {
    return await user.save();
}

module.exports.create = create;
module.exports.login = login;