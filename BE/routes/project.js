var express = require('express');
var router = express.Router();
var projectController = require("../controllers/projectController");

/* GET users listing. */
router.route('/create').post(projectController.createProject);
router.route('/:id').put(projectController.updateProject);
router.route('/:id').delete(projectController.removeProject);
router.route('/user/:userId').get(projectController.getProjectsByUserId);

/* TAKS */
router.route('/:projectId/task/create').post(projectController.addTaskToProject);
router.route('/:projectId/task/:taskId').delete(projectController.removeTaskToProject);
router.route('/:projectId/task/:taskId').put(projectController.editTaskToProject);
router.route('/:projectId/task/:taskId/finish').post(projectController.finishTaskToProject);

module.exports = router;
    