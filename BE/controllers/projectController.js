var projectModel = require("../models/projectModel");

async function createProject(req, res, next) {
    sendResponse(req, res, next, await projectModel.create(req.body));
}

async function updateProject(req, res, next) {
    sendResponse(req, res, next, await projectModel.update(req.params.id,req.body));
}

async function removeProject(req, res, next) {
    sendResponse(req, res, next, await projectModel.remove(req.params.id));
}

async function getProjectsByUserId(req, res, next) {
    sendResponse(req, res, next, await projectModel.getProjectsByUserId(req.params.userId));
}

async function addTaskToProject(req, res, next) {
    sendResponse(req, res, next, await projectModel.addTaskToProject(req.params.projectId, req.body));
}

async function removeTaskToProject(req, res, next) {
    sendResponse(req, res, next, await projectModel.removeTaskToProject(req.params.projectId, req.params.taskId));
}

async function editTaskToProject(req, res, next) {
    sendResponse(req, res, next, await projectModel.editTaskToProject(req.params.projectId, req.params.taskId, req.body));
}

async function finishTaskToProject(req, res, next) {
    sendResponse(req, res, next, await projectModel.finishTaskToProject(req.params.projectId, req.params.taskId));
}

function sendResponse(req, res, next, data) {
    if (!data || data.length === 0) { // no data to
        try {
            res.status(HttpStatus.NOT_FOUND).send("NOT FOUND");
            throw new Error('BROKEN')
          } catch (err) {
            next(err)
          }
    } else {
        if (req.accepts("application/json")) {
            res.jsonp(data);
        } else {
            res.set('Content-Type', 'text/xml');
            console.dir(data);
            res.send(json2xml({ response: data }, { header: true }));
        }
    }

}
module.exports.createProject = createProject;
module.exports.updateProject = updateProject;
module.exports.removeProject = removeProject;
module.exports.getProjectsByUserId = getProjectsByUserId;
module.exports.addTaskToProject = addTaskToProject;
module.exports.removeTaskToProject = removeTaskToProject;
module.exports.editTaskToProject = editTaskToProject;
module.exports.finishTaskToProject = finishTaskToProject;