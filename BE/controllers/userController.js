var userModel = require("../models/userModel");

async function createUser(req, res, next){
    sendResponse(req, res, next,await userModel.create(req.body));
}

async function authentication(req, res, next){
    sendResponse(req, res, next,await userModel.login(req.body));
}

function sendResponse(req, res, next, data) {
    if (!data || data.length === 0) { // no data to
        try {
            res.status(HttpStatus.NOT_FOUND).send("NOT FOUND");
            throw new Error('BROKEN')
          } catch (err) {
            next(err)
          }
    } else {
        res.jsonp(data);
        //if (req.accepts("application/json")) {
        //    res.jsonp(data);
        //} else {
        //    res.set('Content-Type', 'text/xml');
        //    console.dir(data);
        //    res.send(json2xml({ response: data }, { header: true }));
        //}
    }

}

module.exports.createUser = createUser;
module.exports.authentication = authentication;