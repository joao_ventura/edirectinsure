import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Project, SubTasks } from '../models/project-model';

@Injectable({ providedIn: 'root' })
export class ProjectService {
  private url = `${environment.endpoint}/project`;

  constructor(private httpClient: HttpClient) {}

  create(data: { name: string; userID: string }): Observable<Project> {
    return this.httpClient
      .post<Project>(`${this.url}/create`, data)
      .pipe(catchError(this.handleError));
  }

  getProjects(userID: string): Observable<Project[]> {
    return this.httpClient
      .get<Project[]>(`${this.url}/user/${userID}`)
      .pipe(catchError(this.handleError));
  }

  updateProjectName(
    newName: { name: string },
    projectID: string
  ): Observable<Project[]> {
    return this.httpClient
      .put<Project[]>(`${this.url}/${projectID}`, newName)
      .pipe(catchError(this.handleError));
  }

  deleteProject(projectID: string): Observable<Project[]> {
    return this.httpClient
      .delete<Project[]>(`${this.url}/${projectID}`)
      .pipe(catchError(this.handleError));
  }

  oneAddTask(projectID: string, name: string): Observable<SubTasks> {
    return this.httpClient
      .post<SubTasks>(`${this.url}/${projectID}/task/create`, {
        description: name
      })
      .pipe(catchError(this.handleError));
  }

  editTaskName(data: {
    projectID: string;
    allTask: SubTasks;
  }): Observable<SubTasks> {
    return this.httpClient
      .put<SubTasks>(
        `${this.url}/${data.projectID}/task/${data.allTask._id}`,
        data.allTask
      )
      .pipe(catchError(this.handleError));
  }

  deleteTaskFromProject(data: {
    projectID: string;
    taskID: string;
  }): Observable<any> {
    console.log(data);
    return this.httpClient
      .delete<any>(`${this.url}/${data.projectID}/task/${data.taskID}`)
      .pipe(catchError(this.handleError));
  }

  finishTask(data: {
    projectID: string;
    taskID: string;
  }): Observable<SubTasks> {
    return this.httpClient
      .post<SubTasks>(
        `${this.url}/${data.projectID}/task/${data.taskID}/finish`,
        {}
      )
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    return throwError('Something bad happened; please try again later.');
  }
}
