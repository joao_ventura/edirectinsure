import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { HostListener, Injectable } from '@angular/core';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { User } from '../models/user-model';

@Injectable({ providedIn: 'root' })
export class UserService {
  private url = `${environment.endpoint}/user`;
  private dataSource = new BehaviorSubject<User>(new User());
  data = this.dataSource.asObservable();

  constructor(private httpClient: HttpClient) {}

  create(user: User): Observable<User> {
    return this.httpClient
      .post<User>(`${this.url}/create`, user)
      .pipe(catchError(this.handleError));
  }

  authenticate(user: User): Observable<User> {
    return this.httpClient
      .post<User>(`${this.url}/authentication`, user)
      .pipe(catchError(this.handleError));
  }

  saveUserData(): void {
    this.data.subscribe((user) =>
      sessionStorage.setItem('User', JSON.stringify(user))
    );
  }

  updatedBehaviorSubject(data: User): void {
    this.dataSource.next(data);
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    return throwError('Something bad happened; please try again later.');
  }
}
