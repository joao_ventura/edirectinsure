import { Component, OnInit } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { User } from './models/user-model';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(private router: Router, public userService: UserService) {
    router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        if (!router.navigated) {
          const data = JSON.parse(
            sessionStorage.getItem('User') || '{}'
          ) as User;
          this.userService.updatedBehaviorSubject(data);
        }
        sessionStorage.clear();
      }
    });
  }

  ngOnInit(): void {
    window.addEventListener('beforeunload', () => {
      this.userService.saveUserData();
    });
  }
}
