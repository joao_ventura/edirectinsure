export class SubTasks {
  _id: string;
  description: string;
  creationDate: any;
  finishDate: any;
  isDone: any;

  constructor(
    description = '',
    creationDate = undefined,
    finishDate = undefined,
    isDone = undefined
  ) {
    this._id = '';
    this.description = description;
    this.creationDate = creationDate;
    this.finishDate = finishDate;
    this.isDone = isDone;
  }
}

export class Project {
  _id: string;
  name: string;
  userID: string;
  tasks: SubTasks[];
  constructor(name = '', userID = '', tasks = []) {
    this._id = '';
    this.name = name;
    this.userID = userID;
    this.tasks = tasks;
  }

  addSubTask(subTasks: SubTasks) {
    this.tasks.push(subTasks);
  }
}
