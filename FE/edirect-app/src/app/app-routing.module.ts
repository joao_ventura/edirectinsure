import { NgModule } from '@angular/core';
import { Router, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  {
    path: 'login',
    loadChildren: () =>
      import('./functional-components/login/login.module').then(
        (m) => m.LoginModule
      )
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./functional-components/home/home.module').then(
        (m) => m.HomeModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
