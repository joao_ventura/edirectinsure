import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CARD_TYPES } from 'src/app/models/card-types.enum';
import { User } from 'src/app/models/user-model';

@Component({
  selector: 'app-simple-card',
  templateUrl: './simple-card.component.html',
  styleUrls: ['./simple-card.component.scss']
})
export class SimpleCardComponent {
  @Input() cardTitle = '';
  @Input() type = CARD_TYPES.login;

  @Output() loginEvent: EventEmitter<any> = new EventEmitter();
  @Output() registrationEvent: EventEmitter<any> = new EventEmitter();
  @Output() onBtnClicked: EventEmitter<any> = new EventEmitter();

  cardType = CARD_TYPES;
  username = '';
  password = '';
  nameProject = '';

  login(): void {
    this.loginEvent.emit(new User(this.username, this.password));
  }

  registration(): void {
    this.registrationEvent.emit(new User(this.username, this.password));
  }

  newProject(): void {
    this.onBtnClicked.emit(this.nameProject);
  }
}
