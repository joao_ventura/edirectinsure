import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Project, SubTasks } from 'src/app/models/project-model';

@Component({
  selector: 'app-list-card',
  templateUrl: './list-card.component.html',
  styleUrls: ['./list-card.component.scss']
})
export class ListCardComponent implements OnInit {
  newTask = '';

  @Input() project: Project = new Project();
  @Output() changeProjectName: EventEmitter<any> = new EventEmitter();
  @Output() deleteAllProject: EventEmitter<any> = new EventEmitter();
  @Output() oneAddTask: EventEmitter<{
    id: string;
    name: string;
  }> = new EventEmitter();
  @Output() updateNameTask: EventEmitter<{
    projectID: string;
    allTask: SubTasks;
  }> = new EventEmitter();
  @Output() deleteTaskFromProject: EventEmitter<{
    projectID: string;
    taskID: string;
  }> = new EventEmitter();
  @Output() finishTask: EventEmitter<any> = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  saveProjectName(projectID: string): void {
    this.changeProjectName.emit({ newName: this.project.name, id: projectID });
  }

  deleteProject(projectID: string): void {
    this.deleteAllProject.emit(projectID);
  }

  addTask(projectID: string): void {
    this.oneAddTask.emit({ id: projectID, name: this.newTask });
  }

  updateTaskName(id: string, task: SubTasks): void {
    this.updateNameTask.emit({
      projectID: id,
      allTask: task
    });
  }

  deleteTask(id: string, task: SubTasks): void {
    this.deleteTaskFromProject.emit({
      projectID: id,
      taskID: task._id
    });
  }

  checkboxChange(event: any): void {
    this.finishTask.emit(event);
  }
}
