import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SimpleCardComponent } from './simple-card/simple-card.component';
import { ListCardComponent } from './list-card/list-card.component';

@NgModule({
  declarations: [SimpleCardComponent, ListCardComponent],
  imports: [CommonModule, FormsModule],
  exports: [SimpleCardComponent, ListCardComponent]
})
export class UiComponentModule {}
