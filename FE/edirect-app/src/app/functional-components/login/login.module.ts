import { NgModule } from '@angular/core';
import { UiComponentModule } from 'src/app/ui-components/ui-components.module';
import { LoginRoutingModule } from './login-routing.module';

import { LoginComponent } from './login.component';

@NgModule({
  declarations: [LoginComponent],
  imports: [LoginRoutingModule, UiComponentModule],
  providers: []
})
export class LoginModule {}
