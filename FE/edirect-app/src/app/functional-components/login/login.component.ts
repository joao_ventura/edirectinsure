import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CARD_TYPES } from 'src/app/models/card-types.enum';
import { User } from 'src/app/models/user-model';
import { UserService } from 'src/app/services/user.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  constructor(private userService: UserService, private router: Router) {}

  cardTypes = CARD_TYPES;
  cardTitle = environment.companyProjectName;

  onLogin(credentials: User): void {
    this.userService.authenticate(credentials).subscribe((user) => {
      this.userService.updatedBehaviorSubject(user);
      this.router.navigate(['/home']);
    });
  }

  onRegistration(credentials: User): void {
    this.userService.create(credentials).subscribe((user) => console.log(user));
  }
}
