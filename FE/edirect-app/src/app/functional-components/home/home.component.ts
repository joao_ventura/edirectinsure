import { stringify } from '@angular/compiler/src/util';
import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CARD_TYPES } from 'src/app/models/card-types.enum';
import { Project } from 'src/app/models/project-model';
import { User } from 'src/app/models/user-model';
import { ProjectService } from 'src/app/services/project.sevice';
import { UserService } from 'src/app/services/user.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {
  nabBarTitle = environment.companyProjectName;
  user: any;
  dropdown = false;
  cardTypes = CARD_TYPES;
  projects: Project[] = [];

  constructor(
    private userService: UserService,
    private router: Router,
    private projectService: ProjectService
  ) {}

  ngOnInit(): void {
    this.userService.data.subscribe((user) => (this.user = user));
  }

  ngAfterViewInit(): void {
    this.getProjects();
  }

  logout(): void {
    this.userService.updatedBehaviorSubject({} as User);
    this.router.navigate(['']);
  }

  getProjects(): void {
    this.projectService.getProjects(this.user[0]['_id']).subscribe((data) => {
      this.projects = [...data];
    });
  }

  createProject(event: string): void {
    this.projectService
      .create({ name: event, userID: this.user[0]['_id'] })
      .subscribe((project) => this.projects.push(project));
  }

  updateProjectName(data: any): void {
    this.projectService
      .updateProjectName({ name: data.newName }, data.id)
      .subscribe();
  }

  deleteProject(data: any): void {
    this.projectService.deleteProject(data).subscribe((item) => {
      this.projects.splice(
        this.projects.findIndex((e) => e._id === data),
        1
      );
    });
  }

  oneAddTask(data: any): void {
    this.projectService.oneAddTask(data.id, data.name).subscribe((res) => {
      let project: any;
      // eslint-disable-next-line prefer-const
      project = this.projects.find((e) => e._id === data.id);
      console.log(res);
      project.tasks.push(res);
    });
  }

  updateNameTask(data: any): void {
    this.projectService.editTaskName(data).subscribe();
  }

  deleteTaskFromProject(data: any): void {
    this.projectService.deleteTaskFromProject(data).subscribe((res) => {
      let index: number;
      // eslint-disable-next-line prefer-const
      index = this.projects.findIndex((e) => e._id === data.projectID);
      this.projects[index].tasks.splice(
        this.projects[index].tasks.findIndex((e) => e._id === data.taskID),
        1
      );
    });
  }

  finishTask(data: any): void {
    this.projectService.finishTask(data).subscribe();
  }
}
