import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UiComponentModule } from 'src/app/ui-components/ui-components.module';
import { HomeRoutingModule } from './home-routing.module';

import { HomeComponent } from './home.component';

@NgModule({
  declarations: [HomeComponent],
  imports: [HomeRoutingModule, UiComponentModule, CommonModule],
  providers: []
})
export class HomeModule {}
